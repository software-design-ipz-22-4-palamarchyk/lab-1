﻿using System;
using System.Collections.Generic;

interface IAnimal
{
    void MakeSound();
}

class Lion : IAnimal
{
    public void MakeSound()
    {
        Console.WriteLine("Raaar!");
    }
}

class Tiger : IAnimal
{
    public void MakeSound()
    {
        Console.WriteLine("Roar!");
    }
}

class Zebra : IAnimal
{
    public void MakeSound()
    {
        Console.WriteLine("Neigh!");
    }
}

class Elephant : IAnimal
{
    public void MakeSound()
    {
        Console.WriteLine("Trumpet!");
    }
}

interface IEnclosure
{
    int Capacity { get; }
}

class LionEnclosure : IEnclosure
{
    public int Capacity { get { return 5; } }
}

class TigerEnclosure : IEnclosure
{
    public int Capacity { get { return 5; } }
}

class ZebraEnclosure : IEnclosure
{
    public int Capacity { get { return 5; } }
}

class ElephantEnclosure : IEnclosure
{
    public int Capacity { get { return 5; } }
}

interface IZooKeeper
{
    void PerformDuty();
}

class Veterinarian : IZooKeeper
{
    public void PerformDuty()
    {
        Console.WriteLine("Checking animal health");
    }
}

class Cleaner : IZooKeeper
{
    public void PerformDuty()
    {
        Console.WriteLine("Cleaning zoo facilities");
    }
}

class Feeder : IZooKeeper
{
    public void PerformDuty()
    {
        Console.WriteLine("Feeding animals");
    }
}

interface IInventoryItem
{
    void UseItem();
    int Quantity { get; }
}

class FirstAidKit : IInventoryItem
{
    public int Quantity { get { return 3; } }

    public void UseItem()
    {
        Console.WriteLine("Using first aid kit");
    }
}

class EnclosureKeys : IInventoryItem
{
    public int Quantity { get { return 5; } }

    public void UseItem()
    {
        Console.WriteLine("Unlocking enclosures");
    }
}

class Broom : IInventoryItem
{
    public int Quantity { get { return 2; } }

    public void UseItem()
    {
        Console.WriteLine("Sweeping the area");
    }
}

class AnimalFood
{
    public string Name { get; set; }
    public int Quantity { get; set; }

    public AnimalFood(string name, int quantity)
    {
        Name = name;
        Quantity = quantity;
    }
}

class Inventory
{
    private List<IAnimal> animals;
    private List<IZooKeeper> employees;
    private List<IInventoryItem> inventoryItems;
    private List<AnimalFood> animalFood;

    public Inventory(List<IAnimal> animals, List<IZooKeeper> employees, List<IInventoryItem> inventoryItems, List<AnimalFood> animalFood)
    {
        if (animals == null || employees == null || inventoryItems == null || animalFood == null)
        {
            throw new ArgumentNullException("One or more parameters are null.");
        }

        this.animals = animals;
        this.employees = employees;
        this.inventoryItems = inventoryItems;
        this.animalFood = animalFood;
    }

    public void DisplayInfo()
    {
        Console.WriteLine($"Number of animals: {animals.Count}");
        Console.WriteLine($"Number of employees: {employees.Count}\n");
        Console.WriteLine("Animal Food Inventory:");
        foreach (var food in animalFood)
        {
            Console.WriteLine($"{food.Name}: {food.Quantity}");
        }
        Console.WriteLine("\nEmployee Inventory:");
        foreach (var employee in employees)
        {
            Console.WriteLine($"{employee.GetType().Name}");
        }
        Console.WriteLine("\nInventory Items:");
        foreach (var item in inventoryItems)
        {
            Console.WriteLine($"{item.GetType().Name}: {item.Quantity}");
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        Lion lion = new Lion();
        Tiger tiger = new Tiger();
        Zebra zebra = new Zebra();
        Elephant elephant = new Elephant();

        LionEnclosure lionEnclosure = new LionEnclosure();
        TigerEnclosure tigerEnclosure = new TigerEnclosure();
        ZebraEnclosure zebraEnclosure = new ZebraEnclosure();
        ElephantEnclosure elephantEnclosure = new ElephantEnclosure();

        Veterinarian veterinarian = new Veterinarian();
        Cleaner cleaner = new Cleaner();
        Feeder feeder = new Feeder();

        FirstAidKit firstAidKit = new FirstAidKit();
        EnclosureKeys enclosureKeys = new EnclosureKeys();
        Broom broom = new Broom();

        AnimalFood lionFood = new AnimalFood("Meat", 100);
        AnimalFood tigerFood = new AnimalFood("Raw meat", 120);
        AnimalFood zebraFood = new AnimalFood("Grass", 200);
        AnimalFood elephantFood = new AnimalFood("Hay", 300);

        List<IAnimal> animals = new List<IAnimal> { lion, tiger, zebra, elephant };
        List<IZooKeeper> employees = new List<IZooKeeper> { veterinarian, cleaner, feeder };
        List<IInventoryItem> inventoryItems = new List<IInventoryItem> { firstAidKit, enclosureKeys, broom };
        List<AnimalFood> foodItems = new List<AnimalFood> { lionFood, tigerFood, zebraFood, elephantFood };

        Inventory inventory = new Inventory(animals, employees, inventoryItems, foodItems);

        // Виведемо інформацію з інвентарю
        inventory.DisplayInfo();

        Console.ReadKey();
    }
}
